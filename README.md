# ArchLinux-Zen-ChickenIQ
This is a custom version of Arch Linux using kde, simplified for new users.

**Note: This Project is still work in progress.**

Latest ISO: https://drive.google.com/file/d/1_xqIKXWAiXml9Pfh1fCStVFjZRLT7IMF

This project is based off https://github.com/arch-linux-calamares-installer/alci-iso-zen

This is not a very minimal install.

**Packages:**
* Ark
* AppImageLauncher
* Btop
* Discord
* Dolphin
* Firefox
* Fish
* Gwenview
* Htop
* Konsole
* Kvantum Manager
* Lutris
* Mpv
* Nvidia Drivers
* NoiseTorch
* Octopi
* Steam
* Stacer
* Spectacle
* Timeshift
* Unrar
* Vim
* WineGE

**Included:**
* Calamares Installer
* ChaoticAUR

**Todo:**
- [ ] Add a Wallpaper
- [ ] Add a Grub Theme
